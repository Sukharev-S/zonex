import vars from '../_vars'
import {fetchProducts, renderProducts, removeProducts, showProducts, disableProducts} from "../functions/productActions"
import {controlLoadElement} from '../functions/controlLoadElement'
import {pagination, listenerPagination} from "../functions/pagination"

if (document.querySelector('.catalog')) {

    let displayedCatalogGoods = 0,                  // уже показанных товаров
        expectedCatalogGoods = 12,                  // еще ожидаемых к показу товаров
        productsPerPage = 12,                       // товаров на странице
        dataStore = [],                             // массив товаров
        quantityPage = 1,                           // всего количество страниц на найденные товары
        catalogProductUrl = 'jsonResponses/products.json'   // адрес запроса для данных

    // ------- функция загрузки товаров на странице каталога
    const loadProductsCatalog = function() {
        return fetchProducts(catalogProductUrl)
            .then(data => {
                dataStore = [...data]
                return renderProducts(data, vars.$catalogGridList, 'catalog-grid__item', displayedCatalogGoods, expectedCatalogGoods)
            })
            .then(goods => Promise.all(goods.map(controlLoadElement)))
            .then((images) => {
                let newProducts = images.map(el => el.closest('.catalog-grid__item'))
                disableProducts()
                showProducts(newProducts, 100, 'emergence')
            })
    }

    // ------- функция перезагрузки товаров на странице каталога
    const reloadProductsCatalog = function() {
        return removeProducts(vars.$catalogGridList.querySelectorAll('.catalog-grid__item'), 100 , 'emergence')
            .then(() => {
                vars.$catalogGridList.dataset.gridColumns =
                    vars.$catalogColumns.querySelector('.catalog-columns__btn--current').dataset.columns
                loadProductsCatalog().catch(err => console.error(err))
            })
    }

    // --- обработчик кнопок выбора кол-ва столбцов товара
    vars.$catalogColumns.addEventListener('click', e => {
        if (e.target.closest('.catalog-columns__item')) {
            let $columnsBtn = document.querySelectorAll('.catalog-columns__btn')
            $columnsBtn.forEach(el => {
                el.classList.remove('catalog-columns__btn--current')
            })
            e.target.classList.add('catalog-columns__btn--current')
            vars.$customPagination.querySelector('.custom-pagination__link--current').classList
                .remove('custom-pagination__link--current')
            vars.$customPagination.querySelector('.js-custom-pagination__link--begin').classList
                .add('custom-pagination__link--current')
            vars.$customPagination.querySelector('.js-custom-pagination__link--one').textContent = '2'
            vars.$customPagination.querySelector('.js-custom-pagination__link--two').textContent = '3'
            vars.$customPagination.querySelector('.js-custom-pagination__link--tree').textContent = '4'
            displayedCatalogGoods = 0
            productsPerPage = e.target.dataset.columns * 3                     //-- по 3 ряда
            quantityPage = Math.ceil(dataStore.length / productsPerPage)
            expectedCatalogGoods = displayedCatalogGoods + productsPerPage
            reloadProductsCatalog()
                .then(() => pagination.renderPagination(quantityPage))
                .catch(err => console.error(err))
        }
    })

    listenerPagination(pagination)  // выполниться первым, чтобы назначился класс current на кнопки

    vars.$customPagination.addEventListener('click', e => {
        let activeBtn = vars.$customPagination.querySelector('.custom-pagination__link--current')
        if ((e.target.classList.contains('custom-pagination__link'))
            && (!e.target.closest('.js-custom-pagination__item--more-prev'))
            && (!e.target.closest('.js-custom-pagination__item--more-next'))
            && (parseInt(e.target.textContent) !== expectedCatalogGoods / productsPerPage)) {
            displayedCatalogGoods = parseInt(activeBtn.textContent) * productsPerPage - productsPerPage
            expectedCatalogGoods = displayedCatalogGoods + productsPerPage
            reloadProductsCatalog().catch(err => console.error(err))
        }
    })

    loadProductsCatalog()
        .then(() => {
            quantityPage = Math.ceil(dataStore.length / productsPerPage)
            pagination.renderPagination(quantityPage)
        })
        .catch(err => console.error(err))
}