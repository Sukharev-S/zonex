import vars from '../_vars'
import {stepper} from '../functions/stepper'

if (vars.$stepper) {
    const stepperInput = vars.$stepper.querySelector('.stepper__input'),
          stepperMinus = vars.$stepper.querySelector('.stepper__btn--minus'),
          stepperPlus = vars.$stepper.querySelector('.stepper__btn--plus')

    stepper(stepperInput, stepperMinus, stepperPlus)
}

if (document.querySelector('.card')) {
    const prop = vars.$cardInfo.querySelector('.card-info__prop'),
        mainImage = vars.$cardSlider.querySelector('.card-slider__main img'),
        cardSliderThumbs = vars.$cardSlider.querySelector('.card-slider__thumbs'),
        title = vars.$cardInfo.querySelector('.card-info__title'),
        price = vars.$cardInfo.querySelector('.info-price__current'),
        oldPrice = vars.$cardInfo.querySelector('.info-price__old')

    let card = JSON.parse(localStorage.getItem('card')) || {}
    mainImage.setAttribute('src', card.img)
    title.textContent = card.title
    price.textContent = card.price
    price.dataset.usdValue = card.priceUsdValue
    oldPrice.textContent = card.oldPrice
    oldPrice.dataset.usdValue = card.oldPriceUsdValue
    vars.$cardInfo.dataset.id =  card.id
    if (card.isNew) {
        prop.textContent = 'New'
        prop.classList.value = 'card-info__prop product-prop new'
    }
    if (card.isBest) {
        prop.textContent = 'Best seller'
        prop.classList.value = 'card-info__prop product-prop new'
    }
    if (card.isSale) {
        prop.textContent = 'Hot'
        prop.classList.value = 'card-info__prop product-prop new'
    }

    // ------- переключение второстепенной картинки на основную по клику
    cardSliderThumbs.addEventListener('click', e => {
        if (e.target.classList.contains('card-slider__thumb')) {
            let src = e.target.querySelector('img').getAttribute('src')
            mainImage.setAttribute('src', src)
        }
    })
}