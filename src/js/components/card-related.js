import vars from "../_vars";
import {fetchProducts, renderProducts, showProducts} from "../functions/productActions";
import Swiper from "../vendor/swiper.min";
import {controlLoadElement} from "../functions/controlLoadElement";
import {disableProducts} from "../functions/productActions";

let quantityProductShown = 10   // кол-во выводимых продуктов в слайдере

if (vars.$cardRelatedSlider) {
    //  заполнение товарами блок .card-related
    fetchProducts('jsonResponses/relatedProducts.json')
        .then((data) => {
            return renderProducts(data, vars.$cardRelatedList, 'swiper-slide card-related__item', 0, quantityProductShown)
        })
        .then((goods) => {
            // только для страницы card, инициация слайдера товаров на новых DOM элементах
            const relatedSlider = new Swiper(vars.$cardRelatedSlider, {
                loop: false,
                slidesPerView: 2,
                spaceBetween: 30,
                pagination: {
                    el: '.related-pag',
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    768: {
                        slidesPerView: 4,
                    }
                }
            })
            return Promise.all(goods.map(controlLoadElement))
        })
        .then((images) => {
            let newProducts = images.map(el => el.closest('.card-related__item'))
            disableProducts()
            showProducts(newProducts, 100, 'emergence')
        }).catch(err => console.error(err))
}
