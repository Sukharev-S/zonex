import vars from '../_vars';
import {modalWindowOn, modalWindowOff} from '../functions/modalWindows'
import {isValidate} from '../functions/validation'

if (vars.$authorization) {
    const loginForm = vars.$authorizationModal.querySelector('.login-block__form')
    const regForm = vars.$authorizationModal.querySelector('.reg-block__form')
    const errorLoginMessage = vars.$authorizationModal.querySelector('.login-block__message')

    // ------- переключение по табам модального окна авторизации
    const switchingTabs = () => {
        vars.$authorizationModal.querySelectorAll('.authorization-modal__tab').forEach(tab => {
            tab.classList.toggle('authorization-modal__tab--active')
        })
        vars.$authorizationModal.querySelectorAll('.authorization-modal__tab-block').forEach(tab => {
            tab.classList.toggle('authorization-modal__tab-block--fade')
        })
        loginForm.reset()
        regForm.reset()
        errorLoginMessage.classList.add('is-reduce')
    }

    // ------- открытие модального окна авторизации
    vars.$authorization.addEventListener('click', e => {
        modalWindowOn(vars.$authorizationModal, 'authorization-modal--visible')
    })

    // ------- закрытие модального окна авторизации
    vars.$authorizationModal.addEventListener('click', e => {
        if (e.target.closest('.authorization-modal__close') || e.target.classList.contains('authorization-modal')) {
            modalWindowOff(vars.$authorizationModal, 'authorization-modal--visible')
            loginForm.reset()
            regForm.reset()
            errorLoginMessage.classList.add('is-reduce')
        }
        // --- Переключение табов логина и регистрации
        if (e.target.classList.contains('authorization-modal__tab') &&
            !e.target.classList.contains('authorization-modal__tab--active')) {
            switchingTabs()
        }
        // --- клик по кнопке "create an account" модального окна авторизации
        if (e.target.classList.contains('login-block__create-acc')) {
            switchingTabs()
        }
    })

    // ------- submit формы логина модального окна авторизации
    loginForm.addEventListener('submit', e => {
        e.preventDefault()
        errorLoginMessage.classList.remove('login-block__message--success')
        errorLoginMessage.innerHTML = 'Wrong username or password'
        errorLoginMessage.classList.remove('is-reduce')
    })

    // ------- submit формы регистрации модального окна авторизации
    regForm.addEventListener('submit', e => {
        const email = vars.$authorizationModal.querySelector('.reg-block__email')
        const password = vars.$authorizationModal.querySelector('.reg-block__password')
        errorLoginMessage.innerHTML = ''
        e.preventDefault()
        setTimeout(() => {
            if (!isValidate(email)) {
                errorLoginMessage.classList.remove('login-block__message--success')
                errorLoginMessage.innerHTML = 'Incorrect email address'
            } else {
                errorLoginMessage.innerHTML = 'Account created successfully'
                errorLoginMessage.classList.add('login-block__message--success')
            }
        }, 300)

        errorLoginMessage.classList.remove('is-reduce')
    })
}