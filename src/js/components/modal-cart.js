import vars from '../_vars'
import {modalWindowOn, modalWindowOff} from '../functions/modalWindows'
import {priceWithoutSpaces, productionPrice, printTotalPrice} from '../functions/priceActions'
import {getTotalQuantity} from '../functions/calculate'

if (vars.$cart) {
    // открытие модального окна корзины (проверка на пустую корзину)
    vars.$cart.addEventListener('click', e => {
        modalWindowOn(vars.$cartModal, 'cart-modal--visible')
        if (vars.$cartModal.querySelectorAll('.cart-modal__product').length === 0) {
            document.querySelector('.cart-modal__empty').classList.add('cart-modal__empty--visible')
        }
    })

    // закрытие модального окна корзины
    vars.$cartModal.addEventListener('click', e => {
        if (e.target.closest('.cart-modal__close')) {
            modalWindowOff(vars.$cartModal, 'cart-modal--visible')
        }
    })
}

const cartProductsList = vars.$cartModal.querySelector('.cart-modal__list'), // блок списока товаров в модульной корзине
      cartQuantityOut = vars.$cart.querySelector('.cart__quantity'), // элемент маленькая цифра под корзиной
      cartQuantityInto = vars.$cartModal.querySelector('.cart-modal__title'), // элемент кол-во товаров в корзине вверху модальной корзины
      cartTotalPrice = vars.$cartModal.querySelector('.cart-modal__counter'), // элемент итоговой цены в модульной корзине
      cartEmpty = vars.$cartModal.querySelector('.cart-modal__empty') // элемент пустой модульной корзины
let localStore = JSON.parse(localStorage.getItem('cart')) || [],
    currency = localStorage.getItem('currency') || '$'

// проверка на пустую корзину(модальную)
const controlEmptyModalCart = (quantity) => {
    if (quantity === 0) {
        cartEmpty.classList.add('cart-modal__empty--visible')
        vars.$cartModal.querySelector('.cart-modal__list').classList.add('is-hidden')
        vars.$cartModal.querySelector('.cart-modal__bottom').classList.add('is-hidden')
        vars.$cartModal.querySelector('.cart-modal__ad').classList.add('is-hidden')
        cartQuantityOut.classList.add('is-hidden')
    } else {
        cartEmpty.classList.remove('cart-modal__empty--visible')
        vars.$cartModal.querySelector('.cart-modal__list').classList.remove('is-hidden')
        vars.$cartModal.querySelector('.cart-modal__bottom').classList.remove('is-hidden')
        vars.$cartModal.querySelector('.cart-modal__ad').classList.remove('is-hidden')
        cartQuantityOut.classList.remove('is-hidden')
    }
}

// вывод счетчика товаров в корзине под значком корзины и в самой корзине(модальной)
const printQuantity = () => {
    localStore = JSON.parse(localStorage.getItem('cart')) || []
    let quantity = getTotalQuantity()
    cartQuantityOut.textContent = quantity
    cartQuantityInto.textContent = `Cart (${quantity})`
    controlEmptyModalCart(quantity)
}

// рендер добавляемого товара в корзине(модальной)
const htmlModalCartItem = (currency, id, img, title, priceString, quantity, priceUsdValue) => {
    return `
    <li class="cart-modal__product-wrapper">
        <article class="cart-modal__product" data-id="${id}">
            <a href="card.html" class="cart-modal__product-media">
                <img src="${img}" alt="product">
            </a>
            <div class="cart-modal__product-detail">
                <h3 class="cart-modal__product-name">
                    <a href="card.html">${title}</a>
                </h3>
                <div class="cart-modal__product-info">
                    <span class="cart-modal__product-quantity">Qty: ${quantity}</span>     
                    <span class="cart-modal__product-cu js-conventional-unit">${currency}</span>               
                    <span class="cart-modal__product-cost" data-usd-value=${priceUsdValue}>${priceString}</span>
                </div>
            </div>
            <button class="cart-modal__product-btn-remove btn-reset">
                <svg class="cart-modal__remove-icon">
                    <use xlink:href="img/sprite.svg#close" aria-label="remove product"></use>
                </svg>
            </button>
        </article>
    </li>
`
}

// рендер окна сообщения при добавлении товара в корзину
const htmlMessageBox = (currency, id, img, title, priceNumber, priceString, quantity) => {
    if (id) {
        return `
            <div class="message-to-cart__content">
                <div class="message-to-cart__image">
                    <img src="${img}" alt="product">
                </div>
                <div class="message-to-cart__info">
                    <h3 class="message-to-cart__title">-- ${title}</h3>
                    <div class="message-to-cart__price-wrap">
                        <span class="message-to-cart__cu js-conventional-unit">${currency}</span>
                        &nbsp
                        <span class="message-to-cart__price" data-usd-value=${priceNumber}>${priceString}</span>
                        <span> &nbsp&nbsp x ${quantity}</span>
                    </div>
                    <div class="message-to-cart__text"> Added to cart!</div>
                </div>
                <a class="message-to-cart__ref" href="cart.html"></a>
            </div>
        `
    } else return `<div class="message-to-cart__already">This item is already in the cart</div>`
}

// функция получения информации о добавляемом товаре в корзину
const getInfoProduct = (target) => {
    if (target.closest('.card-info')) {  // если клик добавить в корзину был со страницы товара
        let id = vars.$cardInfo.dataset.id,
            img = vars.$cardSlider.querySelector('.card-slider__main img').getAttribute('src'),
            title = vars.$cardInfo.querySelector('.card-info__title').textContent,
            priceNumber = parseFloat(priceWithoutSpaces(vars.$cardInfo.querySelector('.info-price__current').textContent)).toFixed(2),
            priceString = productionPrice(priceNumber),
            quantity = parseInt(vars.$cardInfo.querySelector('.stepper__input').value),
            priceUsdValue = priceWithoutSpaces(vars.$cardInfo.querySelector('.info-price__current').dataset.usdValue),
            prop = '',
            oldPrice = parseFloat(priceWithoutSpaces(vars.$cardInfo.querySelector('.info-price__old').textContent)).toFixed(2),
            oldPriceUsdValue = vars.$cardInfo.querySelector('.info-price__old').dataset.usdValue
        if (vars.$cardInfo.querySelector('.product-prop')) {
            prop = vars.$cardInfo.querySelector('.product-prop').textContent
        }
        return {id, img, title, priceNumber, priceString, quantity, priceUsdValue, prop, oldPrice, oldPriceUsdValue}
    } else {
        let self = target,
            parent = self.closest('.product'),
            id = parent.dataset.id,
            img = parent.querySelector('.product__image img').getAttribute('src'),
            title = parent.querySelector('.product__title a').textContent,
            priceNumber = parseFloat(priceWithoutSpaces(parent.querySelector('.product__price').textContent)).toFixed(2),
            priceString = productionPrice(priceNumber),
            quantity = 1,
            priceUsdValue = priceWithoutSpaces(target.closest('.product').querySelector('.product__price').dataset.usdValue),
            prop = '',
            oldPrice = parseFloat(priceWithoutSpaces(parent.querySelector('.product__old-price').textContent)).toFixed(2),
            oldPriceUsdValue = parent.querySelector('.product__old-price').dataset.usdValue
        if (parent.querySelector('.product-prop')) {
            prop = parent.querySelector('.product-prop').textContent
        }
        return {id, img, title, priceNumber, priceString, quantity, priceUsdValue, prop, oldPrice, oldPriceUsdValue}
    }
}

let timeOutMessage      // флаг наличия сообщения в активной форме
let messageArray = []   // очередь сообщений для отображения
const showMessageCart = (messageBox) => {
    let currency = localStorage.getItem('currency') || '$'
    if (messageArray[0]) {
        const {id, img ,title, priceNumber, priceString, quantity} = messageArray[0]
        messageBox.innerHTML = htmlMessageBox(currency, id, img, title, priceNumber, priceString, quantity)
    }
    messageBox.classList.add('message-to-cart--show')
    return new Promise((resolve) => {
        timeOutMessage = window.setTimeout(() => {
            messageBox.classList.remove('message-to-cart--show')
            resolve()
            timeOutMessage = null
            messageArray.shift()
        }, 2000)  // время на показ сообщения
    })
    .then(() => {
        timeOutMessage = window.setTimeout(() => {
            timeOutMessage = null
            if (messageArray.length > 0) {
                return showMessageCart(messageBox)
            }
        }, 750)  // время на анимацию скрытия сообщения
    })
}

// добавление в корзину(модальную) самого выбранного товара
const addProduct = (target) => {
    let product = getInfoProduct(target),
        localStore = JSON.parse(localStorage.getItem('cart')) || [],
        currency = localStorage.getItem('currency') || '$'
    const messageBox = document.querySelector('.message-to-cart')
    const {id, img ,title, priceNumber, priceString, quantity, priceUsdValue} = product
    if (localStore.find(a =>
        JSON.stringify(a).split('"')[3] === JSON.stringify(product).split('"')[3])) {
        messageBox.innerHTML = htmlMessageBox()
        if (!timeOutMessage) {
            showMessageCart(messageBox).catch(err => console.error(err))
        }
    } else {
        localStore.push(product)
        localStorage.setItem('cart', JSON.stringify(localStore))
        cartProductsList.querySelector('.simplebar-content')
            .insertAdjacentHTML('beforeend', htmlModalCartItem(currency, id, img ,title, priceString, quantity, priceUsdValue))
        printQuantity()
        messageArray.push({id, img ,title, priceNumber, priceString, quantity})
        if (!timeOutMessage) {
            showMessageCart(messageBox).catch(err => console.error(err))
        }
        printTotalPrice(cartTotalPrice)
    }
    target.disabled = true
}

//  удаление из корзины(модальной) выбранного товара
const deleteFromModalCart = (productWrapper) => {
    let product = productWrapper.querySelector('.cart-modal__product'),
        id = productWrapper.querySelector('.cart-modal__product').dataset.id,
        currentPrice = Number(priceWithoutSpaces(productWrapper.querySelector('.cart-modal__product-cost').textContent))
    let localStore = JSON.parse(localStorage.getItem('cart')) || []
    localStore.forEach((a, i) => {
        if (a.id === id) {
            localStore.splice(i, 1)
            currentPrice *= a.quantity
        }
    })
    localStorage.setItem('cart', JSON.stringify(localStore))
    if (document.querySelector(`.product[data-id="${id}"]`)) {
        document.querySelector(`.product[data-id="${id}"]`).querySelector('.to-cart').disabled = false
    }
    printTotalPrice(cartTotalPrice)
    productWrapper.classList.add('cart-modal__product-wrapper--remove')
    product.classList.add('cart-modal__product--remove')
    setTimeout(() => {     // требуется ожидание пока анимируется класс удаления
        productWrapper.remove()
        printQuantity()
    }, 500)
}

// ------- клик по самому товару и добавление в корзину на все блоки где есть перечень товаров
const eventAddToCart = (block) => {
    if (block) {
        block.addEventListener('click', el => {
            // ------- Если клик по кнопке добавить в корзину
            if (el.target.classList.contains('to-cart')){
                addProduct(el.target)
            }
            if (el.target.classList.contains('card-info__btn--tocart')){
                addProduct(el.target)
                setTimeout(() => el.target.disabled = false, 2000)
            }
            // ------- Если клик по самому товару
            if (el.target.closest('[href="card.html"]')) {
                let product, id, img, title, price, priceUsdValue, oldPrice, oldPriceUsdValue, prop,
                    isNew = false,
                    isBest = false,
                    isSale = false,
                    card = {}
                if (el.target.closest('.product')) { // если клик был на товаре сгенерированном по шаблону
                    product = el.target.closest('.product')
                    id = product.dataset.id
                    img = product.querySelector('.product__image img').getAttribute('src')
                    title = product.querySelector('.product__title a').textContent
                    price = product.querySelector('.product__price').textContent
                    priceUsdValue = product.querySelector('.product__price').dataset.usdValue
                    oldPrice = product.querySelector('.product__old-price').textContent
                    oldPriceUsdValue = product.querySelector('.product__old-price').dataset.usdValue
                    if (product.querySelector('.product-prop')) {
                        prop = product.querySelector('.product-prop').textContent
                    }
                }
                if (el.target.closest('.cart-modal__product')) {  // если клик был на товаре из модальной корзины
                    let localStore = JSON.parse(localStorage.getItem('cart')) || []
                    id = el.target.closest('.cart-modal__product').dataset.id
                    localStore.forEach((a, i) => {
                        if (a.id === id) {
                            img = a.img
                            title = a.title
                            price = a.priceNumber
                            priceUsdValue = a.priceUsdValue
                            oldPrice = a.oldPrice
                            oldPriceUsdValue = a.oldPriceUsdValue
                            prop = a.prop
                        }
                    })
                }
                if (prop.toLowerCase() === 'Best seller'.toLowerCase()) isBest = true
                if (prop.toLowerCase() === 'New'.toLowerCase()) isNew = true
                if (prop.toLowerCase() === 'Hot'.toLowerCase()) isSale = true

                card = {id, img, title, price, oldPrice, priceUsdValue, oldPriceUsdValue, isNew, isBest, isSale}
                localStorage.setItem('card', JSON.stringify(card))
            }
        }, false)
    }
}

// ------- клик по кнопке удалить в модальной корзине
cartProductsList.addEventListener('click', el => {
    if (el.target.closest('.cart-modal__product-btn-remove')) {
        deleteFromModalCart(el.target.closest('.cart-modal__product-wrapper'))
    }
})

// ------- наведение курсора на блок товаров в модальной корзине
cartProductsList.addEventListener('mouseover', el => {
    if (el.target.closest('.cart-modal__product-btn-remove')) {
        el.target.closest('.cart-modal__product').querySelector('.cart-modal__product-media').classList.add('cart-modal__product--remove-hover')
        el.target.closest('.cart-modal__product').querySelector('.cart-modal__product-detail').classList.add('cart-modal__product--remove-hover')
    }
})

// ------- покидание курсора с блока товаров в модальной корзине
cartProductsList.addEventListener('mouseout', el => {
    if (el.target.closest('.cart-modal__product-btn-remove')) {
        el.target.closest('.cart-modal__product').querySelector('.cart-modal__product-media').classList.remove('cart-modal__product--remove-hover')
        el.target.closest('.cart-modal__product').querySelector('.cart-modal__product-detail').classList.remove('cart-modal__product--remove-hover')
    }
})

// ------- клик по кнопке перехода сразу на checkout в модальной корзине
vars.$cartModal.querySelector('.cart-modal__order-btn').addEventListener('click', () => {
    localStorage.setItem('refCheckout', 'true')
})

localStore.forEach(a => {
    printTotalPrice(cartTotalPrice)
    cartProductsList.insertAdjacentHTML('beforeend', htmlModalCartItem(currency, a.id, a.img, a.title, a.priceString, a.quantity, a.priceUsdValue))
})
printQuantity()
document.querySelector('.cart-modal__total-wrapper .js-conventional-unit').textContent = currency

eventAddToCart(vars.$mainProductsList)   // если находимся на главной странице, обработка кнопок на товарах
eventAddToCart(vars.$catalogGridList)    // если находимся на странице каталога, обработка кнопок на товарах
eventAddToCart(vars.$cardRelatedSlider)  // если находимся на странице товара(слайдер), обработка кнопок на товарах
eventAddToCart(vars.$searchModal)        // если находимся в модальном окне поиска
eventAddToCart(vars.$cartModal)          // если находимся в модальном окне корзины
eventAddToCart(vars.$cardInfo)     // если находимся на странице товара(основной товар), обработка кнопки в корзину