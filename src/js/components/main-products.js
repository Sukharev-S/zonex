import vars from '../_vars'
import {throttler as resizeThrottler} from '../functions/trottler'
import {controlLoadElement} from '../functions/controlLoadElement'
import {disableProducts} from '../functions/productActions'
import {fetchProducts, renderProducts, removeProducts, showProducts} from '../functions/productActions'

if (vars.$mainProductsList) {  // если находимся на главной странице
    let currentWidth = window.innerWidth,
        elementsPerLine = currentWidth > 1200 ? 5 : currentWidth < 576 ? 2 : 3, // кол-во отображаемых товаров в строке списка
        expectedProducts = elementsPerLine * 2,                 // кол-во запрошенных на показ товаров (два ряда)
        displayedProducts = 0,                                  // кол-во уже отображенных на экране товаров
        mainProductTabUrl = 'jsonResponses/bestSellersProducts.json',  // адрес выборки данных по товарам (зависит от табов)
        dataStore = []                                       // данные из fetch запроса по товарам

    // ------- перезагрузка товаров на главной странице (при изменении размеров окна)
    const reloadProductMainPage = function() {
        elementsPerLine = window.innerWidth > 1200 ? 5 : window.innerWidth < 576 ? 2 : 3
        if (vars.$mainProductsList) {
            if (currentWidth !== window.innerWidth) {
                currentWidth = window.innerWidth
                displayedProducts = 0
                expectedProducts = elementsPerLine * 2
                removeProducts(vars.$mainProducts.querySelectorAll('.products-grid__item'), 100 , 'emergence')
                    .then(() => loadProductsMainPage())
            }
        }
    }

    // ------- загрузка товаров на главной странице
    const loadProductsMainPage = function() {
        vars.$mainProductsMore.style.display = 'inline-flex'
        fetchProducts(mainProductTabUrl)
            .then((data) => {
                dataStore = [...data]
                return renderProducts(data, vars.$mainProductsList, 'products-grid__item', displayedProducts, expectedProducts)
            })
            .then((goods) => Promise.all(goods.map(controlLoadElement)))
            .then((images) => {
                let newProducts = images.map(el => el.closest('.products-grid__item'))
                let heightProductItem = parseInt(getComputedStyle(newProducts[0].closest('.products-grid__item')).height) +
                    parseInt(getComputedStyle(newProducts[0].closest('.products-grid__item')).marginBottom)
                if (expectedProducts >= dataStore.length) {
                    expectedProducts = dataStore.length
                    vars.$mainProductsMore.style.display = 'none'
                } else {
                    vars.$mainProductsMore.style.display = 'inline-flex'
                }
                vars.$mainProductsList.style.height = heightProductItem * Math.ceil(expectedProducts / elementsPerLine) + 'px'
                disableProducts()
                showProducts(newProducts, 100, 'emergence')
            }).catch(err => console.error(err))
    }

    // ------- изменение размера экрана, с использованием троттлинга
    window.addEventListener("resize", () => resizeThrottler(reloadProductMainPage, 2000), false)

    // ------- клик по кнопке загрузить еще товары на главной странице
    vars.$mainProductsMore.addEventListener('click', e => {
        displayedProducts = expectedProducts
        expectedProducts += elementsPerLine
        loadProductsMainPage()
    })

    // ------- клик по категориям (табам)
    vars.$mainProducts.querySelectorAll('.main-products__btn').forEach( btn => {
        btn.addEventListener('click', e => {
            if (!e.currentTarget.classList.contains('main-products__btn--current')) {
                vars.$mainProducts.querySelectorAll('.main-products__btn').forEach( btn => {
                    btn.classList.remove('main-products__btn--current')
                })
                e.currentTarget.classList.add('main-products__btn--current')
                switch (e.currentTarget.dataset.filter) {
                    case 'best-sellers':
                        mainProductTabUrl = 'jsonResponses/bestSellersProducts.json'
                        break;
                    case 'new-products':
                        mainProductTabUrl = 'jsonResponses/newProducts.json'
                        break;
                    case 'sale-products':
                        mainProductTabUrl = 'jsonResponses/saleProducts.json'
                        break;
                }
                displayedProducts = 0
                expectedProducts = elementsPerLine * 2
                removeProducts(vars.$mainProducts.querySelectorAll('.products-grid__item'), 100 , 'emergence')
                    .then(() => loadProductsMainPage())
                    .catch(err => console.error(err))
            }
        })
    })
    loadProductsMainPage()
}