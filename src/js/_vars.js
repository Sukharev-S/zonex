export default {
  $window: window,
  $document: document,
  $html: document.documentElement,
  $body: document.body,
  $header: document.querySelector('.header'),
  $headerContainer: document.querySelector('.header__container'),
  $overlay: document.querySelector('.overlay'),
  $bannerSlider: document.querySelector('.banner-slider'),
  $marketing: document.querySelector('.marketing'),
  $navModal: document.querySelector('.nav-modal'),
  $burger: document.querySelector('.burger'),
  $nav: document.querySelector('.nav'),
  $cart: document.querySelector('.cart'),
  $cartModal: document.querySelector('.cart-modal'),
  $search: document.querySelector('.shop-nav__link--search'),
  $searchModal: document.querySelector('.search-modal'),
  $authorization: document.querySelector('.shop-nav__link--user'),
  $authorizationModal: document.querySelector('.authorization-modal'),
  $customSelect: document.querySelectorAll('.custom-select'),
  $customPagination: document.querySelector('.custom-pagination'),
  $stepper: document.querySelector('.stepper'),
  $freeDelivery: document.querySelector('.free-delivery'),

//-- страница index.html
  $mainProducts: document.querySelector('.main-products'),
  $mainProductsList: document.querySelector('.main-products__list'),
  $mainProductsMore: document.querySelector('.main-products__more'),

//-- страница catalog.html
  $freeDeliveryBtn: document.querySelector('.free-delivery__btn'),
  $catalogSlider: document.querySelector('.hero-catalog__slider'),
  $catalogFiltersTop: document.querySelectorAll('.catalog-filter__top'),
  $hideFilters: document.querySelector('.hide-filters'),
  $catalogColumns: document.querySelector('.catalog-columns__list'),
  $catalogGridList: document.querySelector('.catalog-grid__list'),
  $catalogFilterItems: document.querySelectorAll('.catalog-filter__item'),
  $catalogChoice: document.querySelector('.catalog-choice'),
  $mobileFiltersOpen: document.querySelector('.catalog-mobile-filters'),
  $catalogFilters: document.querySelector('.catalog-filters'),

//-- страница card.html
  $cardInfo: document.querySelector('.card-info'),
  $cardSlider: document.querySelector('.card-slider'),
  $colorSelect: document.querySelector('.color-select'),
  $sizeSelect: document.querySelector('.size-select'),
  $cardDescription: document.querySelector('.card-description'),
  $cardDescriptionLink: document.querySelectorAll('.card-description__link'),
  $cardRelatedSlider: document.querySelector('.card-related__slider'),
  $cardRelatedList: document.querySelector('.card-related__list'),

//-- страница cart.html
  $cartContent: document.querySelector('.cart-content'),
  $cartNavigationList: document.querySelector('.cart-navigation__list'),
  $formCheckout: document.querySelector('.checkout__form'),
  $messageErrorList: document.querySelector('.messages__error-list')
}