import './vendor/focus-visible.min.js'
import './components/header'
import './components/main-slider'
import './components/catalog-slider'
import './components/marketing'
import './components/catalog-filter'
import './components/catalog-grid'
import './functions/stepper'
import './functions/pagination'
import './functions/custom-select'
import SimpleBar from 'simplebar'
import './components/card-select'
import './components/card-product'
import './components/card-description'
import './components/card-related'
import './components/main-products'
import './components/modal-nav'
import './components/mobile-filter'
import './components/modal-cart'
import './components/modal-search'
import './components/modal-authorization'
import './components/cart'
import {currencyConversion} from './functions/convert-currency'

// -------  кастомный скролл в разделе описание товара на странице карточки товара
if (document.querySelector('[data-bar_description]')) {
    new SimpleBar(document.querySelector('.card-description__navigation'))
}

// -------  кастомный скролл в блоке модальной карзины
if (document.querySelector('[data-bar_cart_modal]')) {
    new SimpleBar(document.querySelector('.cart-modal__list'))
}

// -------  кастомный скролл во всех селектах
if (document.querySelector('[data-bar_select]')) {
    document.querySelectorAll('.custom-select__list').forEach(e => new SimpleBar(e))
}

// -------  кастомный скролл в модальном окне поиска
// if (document.querySelector('[data-bar_search]')) {
//     new SimpleBar(document.querySelector('.form-search__results-inner'))
// }

// -------  плавный скрол на всех якорях
const anchors = document.querySelectorAll('a[href^="#"]')
for(let anchor of anchors) {
    anchor.addEventListener("click", function(e) {
        e.preventDefault() // Предотвратить стандартное поведение ссылок
        // Атрибут href у ссылки, если его нет то перейти к body (наверх не плавно)
        let goto = anchor.hasAttribute('href') ? anchor.getAttribute('href') : 'body'
        if (goto !== "#") {//{ goto = '#top'}
            if (document.querySelector(goto)) {
                // Плавная прокрутка до элемента с id = href у ссылки
                document.querySelector(goto).scrollIntoView({
                    behavior: "smooth",
                    block: "start"
                })
            }
        }
    })
}


window.onload = function() {
    // document.querySelectorAll('.js-conventional-unit').forEach(el => {
    //     console.log(el.nextElementSibling.textContent)
    //     el.nextElementSibling.dataset.usdValue = el.nextElementSibling.textContent
    // })
//     currencyConversion()

};