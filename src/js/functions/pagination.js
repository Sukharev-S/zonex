
export const pagination = {
    animateClass : 'emergence',
    pagElement : document.querySelector('.custom-pagination'),
    btnWrapOne : document.querySelector('.js-custom-pagination__item--one'),
    btnWrapTwo : document.querySelector('.js-custom-pagination__item--two'),
    btnWrapTree : document.querySelector('.js-custom-pagination__item--tree'),
    btnWrapBegin : document.querySelector('.js-custom-pagination__item--begin'),
    btnWrapEnd : document.querySelector('.js-custom-pagination__item--end'),
    btnWrapPrev : document.querySelector('.js-custom-pagination__item--prev'),
    btnWrapMorePrev : document.querySelector('.js-custom-pagination__item--more-prev'),
    btnWrapNext : document.querySelector('.js-custom-pagination__item--next'),
    btnWrapMoreNext : document.querySelector('.js-custom-pagination__item--more-next'),
    btnOne : document.querySelector('.js-custom-pagination__link--one'),
    btnTwo : document.querySelector('.js-custom-pagination__link--two'),
    btnTree : document.querySelector('.js-custom-pagination__link--tree'),
    btnBegin : document.querySelector('.js-custom-pagination__link--begin'),
    btnEnd : document.querySelector('.js-custom-pagination__link--end'),

    deleteButtons : function (buttons) {
        const delay = 200
        buttons.forEach((e, i) => {
            setTimeout(() => e.classList.remove(this.animateClass), i * delay)
        })
        return new Promise((resolve) => {
            setTimeout(() => {
                buttons.forEach(e => e.classList.add('is-delete'))
                resolve()
            }, buttons.length * delay + 100)
        })
    },
    showButtons : function (buttons) {
        let delay = 100
        buttons.forEach((e, i) => {
            setTimeout(() => {
                e.classList.remove('is-delete')
                setTimeout(() => e.classList.add(this.animateClass), delay)
            }, i * delay)
        })
    },
    renderButtons : function (buttons) {
        this.deleteButtons(this.pagArrayWraps.filter(el => !buttons.includes(el)))
            .then(() => this.showButtons(this.pagArrayWraps.filter(el => buttons.includes(el))))
            .catch(err => console.error(err))
    },
    renderPagination : function(quantityPage) {
        this.btnEnd.textContent = quantityPage
        if (quantityPage === 1) {
            let visibleButtons = [this.btnWrapBegin]
            this.renderButtons(visibleButtons)
        }
        if (quantityPage === 2) {
            let visibleButtons = [this.btnWrapBegin, this.btnWrapOne, this.btnWrapNext]
            this.renderButtons(visibleButtons)
        }
        if (quantityPage === 3) {
            let visibleButtons = [this.btnWrapBegin, this.btnWrapOne, this.btnWrapTwo, this.btnWrapNext]
            this.renderButtons(visibleButtons)
        }
        if (quantityPage === 4) {
            let visibleButtons = [this.btnWrapBegin, this.btnWrapOne, this.btnWrapTwo, this.btnWrapTree, this.btnWrapNext]
            this.renderButtons(visibleButtons)
        }
        if (quantityPage === 5) {
            let visibleButtons = [this.btnWrapBegin, this.btnWrapOne, this.btnWrapTwo, this.btnWrapTree, this.btnWrapEnd, this.btnWrapNext]
            this.renderButtons(visibleButtons)
        }
        if (quantityPage > 5) {
            let visibleButtons = [this.btnWrapBegin, this.btnWrapOne, this.btnWrapTwo, this.btnWrapTree, this.btnWrapMoreNext, this.btnWrapEnd, this.btnWrapNext]
            this.renderButtons(visibleButtons)
        }
    }
}
// массив оберток кнопок (li) для удобства
pagination.pagArrayWraps = [
    pagination.btnWrapPrev,      // 0
    pagination.btnWrapBegin,     // 1      значение потомка --  1    всегда
    pagination.btnWrapMorePrev,  // 2      значение потомка --  ..   всегда
    pagination.btnWrapOne,       // 3      значение потомка --  2
    pagination.btnWrapTwo,       // 4      значение потомка --  3
    pagination.btnWrapTree,      // 5      значение потомка --  4
    pagination.btnWrapMoreNext,  // 6      значение потомка --  ..   всегда
    pagination.btnWrapEnd,       // 7      значение потомка --  max  всегда.
    pagination.btnWrapNext       // 8
]
// массив числовых кнопок (a) для удобства
pagination.pagArrayButtons = [
    pagination.btnBegin,
    pagination.btnOne,
    pagination.btnTwo,
    pagination.btnTree,
    pagination.btnEnd
]

export const listenerPagination = function(pag) {
    pag.pagElement.addEventListener('click', e => {
        let activeBtn = pag.pagElement.querySelector('.custom-pagination__link--current'),
            nextActiveBtn = ''

        if ((!e.target.classList.contains('custom-pagination__link--current'))
            && (e.target.classList.contains('custom-pagination__link'))
            && (!e.target.closest('.js-custom-pagination__item--more-prev'))
            && (!e.target.closest('.js-custom-pagination__item--more-next'))) {  // если клик по любой рабочей
            if (e.target.closest('.js-custom-pagination__item--next')) {
                nextActiveBtn = pag.pagArrayButtons[pag.pagArrayButtons.indexOf(activeBtn) + 1]
                pag.showButtons([pag.btnWrapPrev])
                if ((nextActiveBtn === pag.btnEnd)
                    && (parseInt(activeBtn.textContent) + 1 < parseInt(pag.btnEnd.textContent))) {
                    ++pag.btnOne.textContent
                    ++pag.btnTwo.textContent
                    ++pag.btnTree.textContent
                } else {
                    activeBtn.classList.remove('custom-pagination__link--current')
                    nextActiveBtn.classList.add('custom-pagination__link--current')
                }
                if (parseInt(pag.btnTree.textContent) + 1 === parseInt(pag.btnEnd.textContent)) {
                    if (pag.btnOne.textContent > 2) pag.showButtons([pag.btnWrapMorePrev])
                    pag.deleteButtons([pag.btnWrapMoreNext])
                }
                if ((pag.btnEnd.classList.contains('custom-pagination__link--current'))
                || ((nextActiveBtn.textContent === pag.btnEnd.textContent))) {
                    pag.deleteButtons([pag.btnWrapNext])
                }
                if (parseInt(pag.btnBegin.textContent) + 2 === parseInt(pag.btnOne.textContent)) {
                    pag.showButtons([pag.btnWrapMorePrev])
                }
            }

            if (e.target.closest('.js-custom-pagination__item--prev')) { // если клик по Prev
                nextActiveBtn = pag.pagArrayButtons[pag.pagArrayButtons.indexOf(activeBtn) - 1]
                pag.showButtons([pag.btnWrapNext])

                if ((nextActiveBtn === pag.btnBegin)
                    && (parseInt(activeBtn.textContent) - 1 > parseInt(pag.btnBegin.textContent))) {
                    --pag.btnOne.textContent
                    --pag.btnTwo.textContent
                    --pag.btnTree.textContent
                } else {
                    activeBtn.classList.remove('custom-pagination__link--current')
                    nextActiveBtn.classList.add('custom-pagination__link--current')
                }
                if (parseInt(pag.btnOne.textContent) - 1 === parseInt(pag.btnBegin.textContent)) {
                    if (pag.btnTree.textContent > 4) pag.showButtons([pag.btnWrapMoreNext])
                    pag.deleteButtons([pag.btnWrapMorePrev])
                }
                if (pag.btnBegin.classList.contains('custom-pagination__link--current')) {
                    pag.deleteButtons([pag.btnWrapPrev])
                }
                if (parseInt(pag.btnTree.textContent) + 2 === parseInt(pag.btnEnd.textContent)) {
                    pag.showButtons([pag.btnWrapMoreNext])
                }
            }

            if (e.target.closest('.js-custom-pagination__item--begin')) {  // если клик по Begin
                activeBtn.classList.remove('custom-pagination__link--current')
                e.target.classList.add('custom-pagination__link--current')
                pag.btnOne.textContent = String(parseInt(pag.btnBegin.textContent) + 1)
                pag.btnTwo.textContent = String(parseInt(pag.btnBegin.textContent) + 2)
                pag.btnTree.textContent = String(parseInt(pag.btnBegin.textContent) + 3)
                pag.deleteButtons([pag.btnWrapMorePrev, pag.btnWrapPrev])
                pag.showButtons([pag.btnWrapNext])
                if (parseInt(pag.btnEnd.textContent) >= 6) {
                    pag.showButtons([pag.btnWrapMoreNext])
                }
            }

            if ((e.target.closest('.js-custom-pagination__item--one'))  // если клик по 1,2,3
                || (e.target.closest('.js-custom-pagination__item--two'))
                || (e.target.closest('.js-custom-pagination__item--tree'))) {
                activeBtn.classList.remove('custom-pagination__link--current')
                e.target.classList.add('custom-pagination__link--current')
                pag.showButtons([pag.btnWrapPrev])
                if (e.target.textContent === pag.btnEnd.textContent) {
                    pag.deleteButtons([pag.btnWrapNext])
                } else pag.showButtons([pag.btnWrapNext])
            }

            if (e.target.closest('.js-custom-pagination__item--end')) {  // если клик по End
                activeBtn.classList.remove('custom-pagination__link--current')
                e.target.classList.add('custom-pagination__link--current')
                pag.btnOne.textContent = String(parseInt(pag.btnEnd.textContent) - 3)
                pag.btnTwo.textContent = String(parseInt(pag.btnEnd.textContent) - 2)
                pag.btnTree.textContent = String(parseInt(pag.btnEnd.textContent) - 1)
                pag.deleteButtons([pag.btnWrapMoreNext, pag.btnWrapNext])
                pag.showButtons([pag.btnWrapPrev])
                if (parseInt(pag.btnEnd.textContent) >= 6) {
                    pag.showButtons([pag.btnWrapMorePrev])
                }
            }
        }
    })
}