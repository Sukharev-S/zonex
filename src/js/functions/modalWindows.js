import vars from "../_vars";

// -------  общая функция открытия модального окна с расчетом ширины скролла при body.overflow
export const modalWindowOn = function(element, animateClass) {
    let bodyWidthBefore = vars.$body.clientWidth
    element.classList.add(animateClass)
    vars.$overlay.classList.add('overlay--visible')
    vars.$body.style.overflow = 'hidden'
    vars.$body.style.paddingRight = vars.$body.clientWidth - bodyWidthBefore + 'px'
    vars.$header.style.paddingRight = vars.$body.style.paddingRight
    if (vars.$freeDelivery) {
        vars.$freeDelivery.style.paddingRight = vars.$body.style.paddingRight
        vars.$freeDeliveryBtn.style.marginRight = vars.$body.style.paddingRight
    }
}

// -------  общая функция закрытия модального окна с расчетом ширины скролла при body.overflow
export const modalWindowOff = function(element, animateClass) {
    element.classList.remove(animateClass)
    vars.$overlay.classList.remove('overlay--visible')
    setTimeout(() => {  // для устранения рывков при удалении паддингов
        vars.$body.style.overflow = 'visible'
        vars.$body.style.paddingRight = '0'
        vars.$header.style.paddingRight = '0'
        if (vars.$freeDelivery) {
            vars.$freeDelivery.style.paddingRight = '0'
            vars.$freeDeliveryBtn.style.marginRight = '0'
        }
    }, 100)
}

// -------  функция для обработчиков закрытия модальных окон
const closeModalWindows = function() {
    modalWindowOff(vars.$navModal, 'nav-modal--visible')
    modalWindowOff(vars.$authorizationModal, 'authorization-modal--visible')
    modalWindowOff(vars.$cartModal, 'cart-modal--visible')
    modalWindowOff(vars.$searchModal, 'search-modal--visible')
    vars.$searchModal.querySelector('.form-search__results-container')
        .classList.remove('form-search__results-container--visible')
}

// -------  обработчик клика на overlay (для модальных окон)
vars.$overlay.addEventListener('click', e => {
    closeModalWindows()
})

// -------  обработчик закрытия модальных окон по нажатию ESCAPE
vars.$body.addEventListener('keydown', e => {
    if (e.code  === 'Escape') {
        closeModalWindows()
    }
})