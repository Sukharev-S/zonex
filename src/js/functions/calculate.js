import {getCurrencyFactor} from "./convert-currency";
import {priceWithoutSpaces} from "./priceActions";

// подсчет итоговой суммы товара в корзине с учетом выбранной валюты
export const getTotalPrice = () => {
    let currencyFactor = getCurrencyFactor(),  // определяет коэффициент установленной валюты к USD
        totalSum = 0,
        localStore = JSON.parse(localStorage.getItem('cart')) || []
    localStore.forEach(el => {
        totalSum += parseInt(el.quantity) * currencyFactor * parseFloat(priceWithoutSpaces(el.priceUsdValue))
    })
    return totalSum
}

// подсчет количества товаров в корзине
export const getTotalQuantity = () => {
    let totalQuantity = 0,
        localStore = JSON.parse(localStorage.getItem('cart')) || []
    localStore.forEach(el => {
        totalQuantity += parseInt(el.quantity)
    })
    return totalQuantity
}